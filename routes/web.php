<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Route::get('/mail_test', 'FormController@mail_test');

Route::get('/{firma}', 'FormController@pokaz_strona_startowa');

Route::get('{firma}/create', 'FormController@pokaz_formularz')->name('posts.showForm');

Route::post('posts', 'FormController@wynik')->name('posts.submit');

Route::get('{firma}/regulamin', 'FormController@show_regulamin');




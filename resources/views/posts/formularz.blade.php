@extends('template')

@section('content')
    @if($errors->any())
        <div class="alert alert-danger row col-12 col-sm-12" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="POST" action="{{ route('posts.submit') }}">
        @csrf

        <div class="row input_mail">
            <label for="inputEmail">Wprowadź adres email:</label>
            <div class="col-sm-8">
                <input type="email" id="inputEmail" name="inputEmail" required>
            </div>
        </div>

        @foreach($pytania as $pytanie)
            <div>
                <div class="pytanie row col-12 col-sm-12">{{$pytanie['tresc']}}</div>
                @foreach($odpowiedzi as $odpowiedz)
                    <div class="form-check row col-12 col-sm-12">
                        <input class="icheck" type="radio" id="{{$loop->parent->iteration}}{{ $odpowiedz['id'] }}radio"
                               name="pyt[{{ $pytanie['id'] }}]"
                               value="{{ $odpowiedz['id'] }}" required>
                        <label class="form-check-label"
                               for="{{$loop->parent->iteration}}{{ $odpowiedz['id'] }}radio">{{$odpowiedz['tresc']}}</label>
                    </div>
                @endforeach
            </div>
        @endforeach
        <div class="zgoda row">
            <input tabindex="9" type="checkbox" id="square-checkbox" class="icheck" value="zgoda" required>
            <label for="square-checkbox-1" class="checkbox_regulamin"> Zapoznałem się z <a href="{{ '/'.$firma.'/regulamin' }}" target="_blank">regulaminem</a></label>
        </div>

        <input type="hidden" value="{{$firma}}" name="firma">
        <input type="submit" value="Prześlij formularz" class="btn btn-secondary przycisk">
    </form>
@endsection('content')

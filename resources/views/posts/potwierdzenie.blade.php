@extends('template')

@section('content')

    <h4>Dziękujemy za wypełnienie ankiety.</h4>
    <p>Na podany adres email została wysłana wiadomość zawierająca informację, czy Państwa dziecko może mieć problem ze wzrokiem oraz sugestie odnośnie dalszego postępowania.

        Jeśli wiadomość nie dotrze, prosimy o sprawdzenie folderu ’spam’ w Państwa skrzynce mailowej lub wypełnienie ankiety ponownie upewniając się, że adres jest wprowadzony poprawnie. Jeśli mail nadal nie dotrze, prosimy o kontakt pod <script>
            var uzytkownik = 'kontakt';
            var domena = 'sprawdzwzrok.pl';
            var dodatkowe = '?subject=Problem z mailem zwrotnym';
            var opis = 'adresem.';
            document.write('<a hr' + 'ef="mai' + 'lto:' + uzytkownik + '\x40' + domena + dodatkowe + '">');
            if (opis) document.write(opis + '<'+'/a>');
            else document.write(uzytkownik + '\x40' + domena + '<'+'/a>');
        </script></p>

@endsection

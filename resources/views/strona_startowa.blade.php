@extends('template')

@section('content')
    <div id="strona_startowa_text">
        <h6>Szanowni Państwo,</h6>
        <p>Oddajemy w Państwa ręce narzędzie, za pomocą którego uzyskacie informację, czy Państwa dziecko może
            mieć problem z widzeniem i czy warto wykonać badanie wzroku. Jest to ankieta nawiązująca do codziennych
            czynności i zachowań oraz częstotliwości ich występowania: od nigdy do zawsze. Przed przystąpieniem do
            jej wypełniania prosimy zapoznać się z pytaniami i zastanowić się, jakie dolegliwości
            zaobserwowaliście Państwo u dziecka w przeciągu ostatniego pół roku.</p>

        <p>Wprowadzane dane są anonimowe, wyniki zostaną wykorzystane do celów statystycznych, natomiast nie będą
            one w żaden sposób powiązane z adresem mailowym, o który prosimy na początku ankiety. Służyć on będzie
            tylko i wyłącznie wysłaniu uzyskanego wyniku z ewentualnymi zaleceniami i poradami. Nie będzie on nigdzie
            zapisany ani przechowywany, nie będzie też służył do przesyłania żadnych dodatkowych informacji czy
            ofert (szczegóły związane z przetwarzaniem danych zawarte są w regulaminie, z którym można zapoznać się
            na stronie ankiety). Podczas wprowadzania adresu mailowego prosimy o upewnienie się, czy został on
            wprowadzony poprawnie, a w przypadku braku informacji zwrotnej prosimy o sprawdzenie folderu ’spam’ w
            Państwa skrzynce mailowej. Jeśli mail nigdzie nie dotrze, prosimy o kontakt pod
            <script>
                var uzytkownik = 'kontakt';
                var domena = 'sprawdzwzrok.pl';
                var dodatkowe = '?subject=Problem z mailem zwrotnym';
                var opis = 'adresem.';
                document.write('<a hr' + 'ef="mai' + 'lto:' + uzytkownik + '\x40' + domena + dodatkowe + '">');
                if (opis) document.write(opis + '<'+'/a>');
                else document.write(uzytkownik + '\x40' + domena + '<'+'/a>');
            </script></p>

        <p>Ankieta powstała na bazie kwestionariusza utworzonego przez College of Optometrists in Vision Science.</p>

        <p class="margin-bottom:0px"><b>Wieńczysław Bloch</b><br>
        właściciel sieci salonów optycznych Bloch Optyk</p>

    </div>

        <button type="button" class="btn btn-secondary przycisk"><a href="{{ '/'.$firma.'/create' }}" class="link">Przejdź
                do ankiety</a></button>

@endsection

<!doctype html>
<html lang="pl">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="{{ asset('skins/square/blue.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700&display=swap"
          rel="stylesheet">

    <title>{{$title}}</title>
</head>
<body>
<div class="container">

    <div id="belka" class="row">
        <div class="polowka poloka_prawa header_text col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 ">
            <h5>Ankieta przesiewowej kontroli wzroku</h5>
            <h5>pod kątem zaburzeń widzenia</h5>
            <h5>dla dzieci w wieku wczesnoszkolnym i szkolnym</h5>
            @if ($logoPL!="")
                <h6>patronat: {{$logoPL}}</h6>
            @endif
        </div>
        <div class="polowka poloka_lewa col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
            @if ($logoPL!="")
                    <img class="img-fluid" style="max-height: 80px; margin-top: 25px; margin-bottom: auto; margin-right: 25px;"
                         src="\images\logos\bloch.png"
                         alt="logo2">
                    <img class="img-fluid" style="max-height: 80px; margin-top: 25px; margin-bottom: auto; margin-left: 25px;" src="{{$logo}}"
                         alt="logo1">
            @else
                <img class="img-fluid img_logo" src="\images\logos\bloch.png" alt="logo2">
            @endif
        </div>
    </div>

    <div class="content">
        @yield('content')
    </div>

    <footer class="footer">
        Copyright © 2020 Kor-Net
    </footer>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

<script src="{{ asset('skins/square/icheck.js') }}"></script>


<script>
    $(document).ready(function () {
        $('.icheck').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('[data-toggle="popover"]').popover();
    });
</script>

</body>
</html>

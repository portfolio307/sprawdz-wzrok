<?php

use App\Models\Odpowiedzi;
use Illuminate\Database\Seeder;

class OdpowiedziSeeder extends Seeder
{
    public function run()
    {
        $questions = [
            ["Nigdy",0],
            ["Rzadko",1],
            ["Czasami",2],
            ["Często",3],
            ["Zawsze",4]
        ];

        foreach ($questions as $question) {
            $a = new odpowiedzi();
            $a->tresc = $question[0];
            $a->wartosc = $question[1];
            $a->save();
        }
    }
}

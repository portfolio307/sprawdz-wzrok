<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(PytaniaSeeder::class);
         $this->call(OdpowiedziSeeder::class);
         $this->call(LogosSeeder::class);
    }
}

<?php

use App\Models\Logo;
use Illuminate\Database\Seeder;

class LogosSeeder extends Seeder
{
    public function run()
    {
        $data = [
            ["plewiska", "", ""],
            ["ankieta", "", ""],
            ["poznan", "Poznań", "\images\logos\poznan.png"]
        ];

        foreach ($data as $key) {
            $a = new Logo();
            $a->firma = $key[0];
            $a->firma_pl = $key[1];
            $a->link = $key[2];
            $a->save();
        }
    }
}

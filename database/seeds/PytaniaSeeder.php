<?php

use App\Models\Pytania;
use Illuminate\Database\Seeder;

class PytaniaSeeder extends Seeder
{
    public function run()
    {
        $questions = [
            "1. Bóle głowy przy czytaniu/nauce",
            "2. Zamazywanie obrazu przy patrzeniu blisko",
            "3. Nadmierne mruganie, tarcie oczu, pieczenie oczu, łzawienie",
            "4. Gubienie się podczas czytania lub przepisywania",
            "5. Nastawianie głowy w różnych kierunkach, przymykanie/przesłanianie jednego z oczu",
            "6. Rozmazywanie obrazu z daleka",
            "7. Unikanie czytania/zasypianie przy czytaniu",
            "8. Pomijanie bądź zastępowanie wyrazów przy czytaniu lub przepisywaniu",
            "9. Brzydki charakter pisma",
            "10. Mylenie liter i cyfr",
            "11. Słabe rozumienie czytanego tekstu",
            "12. Nienaturalne przybliżanie do kartki, żeby coś zapisać lub przeczytać",
            "13. Problemy z utrzymaniem skupienia przy czytaniu",
            "14. Unikanie zadań wymagających koncentracji",
            "15. Zawsze mówi ‚‚nie potrafię’’ zanim jeszcze spróbuje",
            "16. Słaba koordynacja ciała",
            "17. Unikanie sportów oraz zabaw ruchowych",
            "18. Nie potrafi dobrze ocenić odległości ",
            "19. Roztargniony / słaba pamięć"
        ];

        foreach ($questions as $question) {
            $a = new pytania();
            $a->tresc = $question;
            $a->save();
        }
    }
}

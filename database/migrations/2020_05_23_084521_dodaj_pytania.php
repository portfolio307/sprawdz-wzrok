<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DodajPytania extends Migration
{
    public function up()
    {
        Schema::create('pytania', function (Blueprint $table) {
            $table->id();
            $table->string('tresc');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pytania');
    }
}

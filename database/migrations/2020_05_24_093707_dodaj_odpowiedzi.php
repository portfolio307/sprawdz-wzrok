<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DodajOdpowiedzi extends Migration
{
    public function up()
    {
        Schema::create('odpowiedzi', function (Blueprint $table) {
            $table->id();
            $table->string('tresc');
            $table->integer('wartosc');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('odpowiedzi');
    }
}

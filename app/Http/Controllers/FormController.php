<?php

namespace App\Http\Controllers;

use App\Mail\FormSubmitted;
use App\Mail\FormSubmitted_2;
use App\Models\Logo;
use App\Models\Odpowiedzi;
use App\Models\Pytania;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{
    private function getImageLinkForCompany($companyName)
    {
        try {
            $logo = Logo::query()->where('firma', '=', $companyName)->firstOrFail();
            return $logo->link;
        } catch (\Exception $e) {
            return abort(404);
        }
    }

    private function getPolishName($poishName)
    {
        try {
            $logoPL = Logo::query()->where('firma', '=', $poishName)->firstOrFail();
            return $logoPL->firma_pl;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function pokaz_strona_startowa($firma)
    {
        return view('strona_startowa', ["title" => "Ankieta kontroli wzroku Bloch Optyk", "logo" => $this->getImageLinkForCompany($firma), 'firma' => $firma, "logoPL" => $this->getPolishName($firma)]);
    }

    public function pokaz_formularz($firma)
    {
        $pytania = Pytania::all();
        $odpowiedzi = Odpowiedzi::query()->orderByRaw('wartosc')->get();
        return view('posts/formularz', ["title" => "Ankieta kontroli wzroku Bloch Optyk - formularz", "pytania" => $pytania, "odpowiedzi" => $odpowiedzi, "logo" => $this->getImageLinkForCompany($firma), 'firma' => $firma, "logoPL" => $this->getPolishName($firma)]);
    }

    public function wynik(Request $request)
    {
        $answersCount = Pytania::query()->count();

        $request->validate([
            "pyt" => 'required|array|size:' . $answersCount,
            "pyt.*" => 'required|int',
            "inputEmail" => 'email:dns,rfc',
        ]);

        $firma = $request->input('firma');

        $answers = Odpowiedzi::query()->select('id', 'wartosc')->get();
        $odpowiedzi = [];

        foreach ($answers as $answer) {
            $odpowiedzi[$answer->id] = 0;
        }

        foreach ($request->input('pyt') as $pyt_id => $odp_id) {
            $odpowiedzi[$odp_id] = $odpowiedzi[$odp_id] + 1;
        }

        $wynik = 0;
        foreach ($answers as $answer) {
            $wynik += $answer->wartosc * $odpowiedzi[$answer->id];
        }

        //wynik2
        foreach ($answers as $answer) {
            $odpowiedzi[$answer->id] = 0;
        }

        foreach ($request->input('pyt') as $pyt_id => $odp_id) {
            if ($pyt_id == 1 || $pyt_id == 3 || $pyt_id == 6 || $pyt_id == 7 || $pyt_id == 12) {
                $odpowiedzi[$odp_id] = $odpowiedzi[$odp_id] + 1;
            }
        }

        $wynik2 = 0;
        foreach ($answers as $answer) {
            $wynik2 += $answer->wartosc * $odpowiedzi[$answer->id];
        }

        $mail_adress = $request->input(['inputEmail']);

        $odpowiedz_przesylana = "";

        //sprawdzanie z jakiich zakresów są wyniki
        if ($wynik <= 19) {
            $odpowiedz_przesylana = "Gratulacje! Wynik uzyskany w ankiecie nie wskazuje na wadę wzroku bądź problemy z widzeniem obuocznym. Przypominamy o zachowaniu higieny wzrokowej - prawidłowe oświetlenie podczas pisania i czytania, prawidłową odległość przy czytaniu lub korzystaniu ze smartfona/tabletu. Długotrwała praca wzrokowa z bliska zwiększa ryzyko pojawienia się krótkowzroczności, dlatego też  warto wprowadzić zasadę 20/20/20 - przy pracy w bliskiej odległości co 20 minut kierujemy spojrzenie swobodnie przez 20 sekund w dal. Zachęcajmy również dzieci do aktywności fizycznej na świeżym powietrzu, ponieważ przebywanie poza czterema ścianami zmniejsza ryzyko pojawienia się wady wzroku.";
            Mail::to($mail_adress)->send(new FormSubmitted_2($wynik, $wynik2, $odpowiedz_przesylana));
            Mail::to("kontakt@example.pl")->send(new FormSubmitted_2($wynik, $wynik2, $odpowiedz_przesylana));
        } else if ($wynik >= 20 && $wynik <= 24) {
            $odpowiedz_przesylana = "Wynik, który uzyskało Twoje dziecko sugeruje problem ze wzrokiem (nieskorygowana wada wzroku/problem z widzeniem obuocznym). Zalecamy umówienie się na kompleksowe badanie. Dobre widzenie to nie tylko odczytywanie najmniejszych literek z tablicy, ale też sprawna akomodacja, konwergencja i ruchomość oczu - zaburzenia tych parametrów skutkują problemami z wydajnością i skupieniem. Pełne badanie może wykluczyć bądź potwierdzić wadę wzroku, zeza, niedowidzenie, problemy z widzeniem obuocznym. Specjalista zaleci dalszą drogę postępowania.";
            Mail::to($mail_adress)->send(new FormSubmitted($wynik, $wynik2, $odpowiedz_przesylana));
            Mail::to("kontakt@example.pl")->send(new FormSubmitted($wynik, $wynik2, $odpowiedz_przesylana));
        } else if ($wynik >= 25) {
            $odpowiedz_przesylana = "Wynik, który uzyskało Twoje dziecko wskazuje na problem ze wzrokiem (nieskorygowana wada wzroku/problem z widzeniem obuocznym). Nie zwlekaj i umów się na kompleksowe badanie. Dobre widzenie to nie tylko odczytywanie najmniejszych literek z tablicy, ale też sprawna akomodacja, konwergencja i ruchomość oczu - zaburzenia tych parametrów skutkują problemami z wydajnością i skupieniem. Pełne badanie może wykluczyć bądź potwierdzić wadę wzroku, zeza, niedowidzenie, problemy z widzeniem obuocznym. Specjalista zaleci dalszą drogę postępowania.";
            Mail::to($mail_adress)->send(new FormSubmitted($wynik, $wynik2, $odpowiedz_przesylana));
            Mail::to("kontakt@example.pl")->send(new FormSubmitted($wynik, $wynik2, $odpowiedz_przesylana));
        }

        return view('posts/potwierdzenie', [
            "wynik" => $wynik,
            "wynik2" => $wynik2,
            "title" => "Ankieta kontroli wzroku Bloch Optyk - potwierdzenie",
            "logo" => $this->getImageLinkForCompany($firma),
            "logoPL" => $this->getPolishName($firma)
        ]);
    }

    public function show_regulamin($firma)
    {
        return view('regulamin', ["title" => "Ankieta kontroli wzroku Bloch Optyk - regulamin", "logo" => $this->getImageLinkForCompany($firma), 'firma' => $firma, "logoPL" => $this->getPolishName($firma)]);
    }

}


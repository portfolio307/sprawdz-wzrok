<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Odpowiedzi extends Model
{
    protected $table = 'odpowiedzi';
    protected $fillable = ['tresc'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pytania extends Model
{
    protected $table = 'pytania';
    protected $fillable = ['tresc'];
}

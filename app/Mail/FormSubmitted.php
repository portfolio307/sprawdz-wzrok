<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FormSubmitted extends Mailable
{
    use Queueable, SerializesModels;

    public $wynik;
    public $wynik2;
    public $odpowiedz_przesylana;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($wynik, $wynik2, $odpowiedz_przesylana)
    {
        $this->wynik = $wynik;
        $this->wynik2 = $wynik2;
        $this->odpowiedz_przesylana = $odpowiedz_przesylana;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('badanie.wzroku@example.pl')
            ->subject('Wynik ankiety kontroli wzroku Bloch Optyk')
            ->view('emails.mail_template');
    }
}
